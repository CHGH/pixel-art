module.exports = {
    'Demo test Google': function(client) {
        client
            .url('http://www.google.com')
            .waitForElementVisible('body', 1000)
            .setValue('input[type=text]', 'nigthwatch')
            .waitForElementVisible('button[name=btnK]', 1000)
            .click('button[name=btnK]')
            .pause()
            .assert.containsText('#main', "Night Watch")
            .end();
    }
};
