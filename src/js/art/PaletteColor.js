export class PaletteColor {
    constructor(color) {
        this.mColor = color;
        this.mElement = document.createElement("div");
        this.mElement.setAttribute("data-color", color);
        this.mElement.setAttribute("class", "paletteColor");
        this.mElement.style.backgroundColor = color;
    }

    get element() {
        return this.mElement;
    }

    get color() {
        return this.mColor;
    }
}
