export class Brush {
    constructor(color) {
        this.mColor = color;
    }

    get color() {
        return this.mColor;
    }

    set color(color) {
        this.mColor = color;
    }
}
