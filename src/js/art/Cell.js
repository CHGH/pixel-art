export class Cell {

    constructor(row, column) {
        this.mElement = document.createElement("td");
        this.mElement.setAttribute("class", "cell");
        this.mElement.setAttribute("id", "r" + row + "c" + column);
        this.mSize = 10;
    }

    get element() {
        return this.mElement;
    }

    set color(color) {
        this.mElement.style.backgroundColor = color;
    }

    get color() {
        return this.mElement.style.backgroundColor;
    }

    get size() {
        return this.mSize;
    }
}
