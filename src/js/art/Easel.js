import {Cell} from './Cell';

export class Easel {

    constructor(width, height) {
        this.mWidth = parseInt(width);
        this.mHeight = parseInt(height);
        this.mEasel = Array(height);
        for (let i = 0; i < height; i++) {
            this.mEasel[i] = Array(width);
        }
        this.fillEasel();
    }

    get height() {
        return this.mHeight;
    }

    get width() {
        return this.mWidth;
    }

    fillEasel() {
        for (let i = 0; i < this.height; i++) {
            for (let j = 0; j < this.width; j++) {
                this.mEasel[i][j] = new Cell(i, j);
            }
        }
    }

    getCell(x, y) {
        return this.mEasel[y][x];
    }

    getRow(y) {
        let row = document.createElement("tr");
        for (let i = 0; i < this.mWidth; i++) {
            row.appendChild(this.getCell(i, y).element);
        }
        return row;
    }
}
