import {PaletteColor} from "./PaletteColor";

export class Palette {
    constructor(colors) {
        if (!(colors instanceof Array)) {
            throw new Error("Colors shoud be array!");
        }
        this.mColors = Array(colors.length);
        for (let i = 0; i < colors.length; i++) {
            this.mColors[i] = new PaletteColor(colors[i]);
        }
    }

    get colors() {
        return this.mColors;
    }
}
