import $ from 'jquery';
import {Easel} from './art/Easel';
import {Brush} from './art/Brush.js';
import {Palette} from './art/Palette.js';
import {Util} from "./util/Util";

let mEasel;
const mEaselView = $("#easel");
const mPalette = new Palette(["#F44336", "#E91E63", "#9C27B0",
    "#673AB7", "#3F51B5", "#2196F3", "#03A9F4", "#00BCD4", "#009688",
    "#4CAF50", "#8BC34A", "#CDDC39", "#FFEB3B", "#FFC107", "#FF9800",
    "#FF5722", "#775447", "#9E9E9E", "#607D8B", "#FFFFFF", "#000"]);
const mPaletteView = document.getElementById("palette");
const mBrush = new Brush(mPalette.colors[Util.prototype.randomInt(mPalette.colors.length)].color);
const mCurrentColorView = document.getElementById("current-color");
const mColorInputView = document.getElementById("color-input");

let easelWidth;

const fInitEasel = (width, height) => {
    console.log("Initializing easel.");

    mEasel = new Easel(width, height);

    for (let i = 0; i < mEasel.height; i++) {
        let row = mEasel.getRow(i);
        mEaselView.append(row);
    }

    easelWidth = width * mEasel.getCell(0, 0).size + width * 3 + 1 + "px";
    mEaselView.css('width', easelWidth + 'px');

    document.querySelectorAll("#easel td").forEach(c => c.addEventListener("click", function () {
        console.log("Clicked!" + this.id);
        this.style.backgroundColor = mBrush.color;
    }));

    console.log("Easel has been initialized!");
};

const fInitPalette = () => {
    console.log("Initializing palette.");

    mPalette.colors.forEach(c => {
        mPaletteView.appendChild(c.element);
        c.element.addEventListener("click", function () {
            mBrush.color = c.element.getAttribute("data-color");
            mCurrentColorView.innerHTML = mBrush.color;
            mColorInputView.value = mBrush.color;
            console.log("Changed brush color to: " + mBrush.color);
        });
    });

    mCurrentColorView.innerHTML = mBrush.color;
    mColorInputView.value = mBrush.color;

    mPaletteView.style.width = easelWidth;

    mColorInputView.addEventListener("change", function (event) {
        mBrush.color = event.target.value;
        mCurrentColorView.innerHTML = event.target.value;
    });

    console.log("Palette has been initialized!");
};

const fInit = () => {
    fInitEasel(32, 18);
    fInitPalette();
};

fInit();
