let webpack = require('webpack');
module.exports = {
    mode: 'production',
    entry: {
        entry: __dirname + '/src/js/main.js'
    },
    output: {
        filename: 'art.bundle.js'
    },
    externals: {
        $: 'jquery',
        react: 'react'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};